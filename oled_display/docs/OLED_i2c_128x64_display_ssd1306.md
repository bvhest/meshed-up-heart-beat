# OLED Specificaties

Specificaties:

 * 128*64 pixels
 * Werkt met 3.3V én 5V (3V-5V, zowel voeding als signalen)
 * Afmetingen: 27,8 x 27.3 x 4.3mm (d x h x b)
 * Werkt op I2C (standaard op adres 0x78)
 * Modificatie mogelijk om scherm op I2C adres 0x7A te zetten

## Pinout

| ssd1306 Display | Wemos D1 (ESP8266) | Wemos LOLIN32 - ESP32 - CP2104 | ESP-WROOM-32 | 
| --------------- | -------------------| ------------ | ------------ |
| GND |	GND | GND | GND |
| VCC |	3.3V | 3.3V | 3.3V |
| SCL |	GPIO5-D1 | GPIO22-SCL (rechts pin16) | GPIO22-SCL (rechts pin14) |
| SDA |	GPIO4-D2 |  GPIO21-SDA (rechts pin15) |  GPIO21-SDA (rechts pin11) |

![pinout OLED screen](OLED-Display-font.jpg)


## Library

Use the MicroPython [ssd1306 display driver](https://github.com/micropython/micropython/blob/master/drivers/display/ssd1306.py).

## Voorbeelden

 * [MicroPython: OLED Display with ESP32 and ESP8266](https://randomnerdtutorials.com/micropython-oled-display-esp32-esp8266/)
 * [Driving I2C OLED displays with MicroPython](https://www.twobitarcade.net/article/oled-displays-i2c-micropython/)
 