# Complete project details at https://RandomNerdTutorials.com

from machine import Pin, I2C
import ssd1306
from time import sleep

# ESP32 Pin assignment 
#i2c = I2C(-1, scl=Pin(22), sda=Pin(21))

# ESP8266 Pin assignment
# our display has a resolution of 128 x 64 
# our display uses address: 0x78 
i2c = I2C(-1, scl=Pin(5), sda=Pin(4))

oled_width = 128
oled_height = 64
display = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

devices = i2c.scan()
print(devices)

oled_width = 128
oled_height = 64
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

oled.text('Hello World 1!', 0, 0)
oled.text('Hello World 2!', 0, 10)
oled.text('Hello World 3!', 0, 20)
        
oled.show()
