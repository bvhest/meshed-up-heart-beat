# TFG meetup - "Meshed-up heart-beat"

## Doel

Voorbereiden van de hardware en software voor één of meerdere meet-ups waarin IoT, data-uitwisseling, data-analyse en visualisatie bij elkaar komen.

Hopelijk geldt dit ook voor kennis-deling en uitwisseling op dit gebied.

## Documentatie

  * [Finding the beat in HR sensor data](https://www.twobitarcade.net/article/wemos-heart-rate-sensor-display-micropython/)
  * [MicroPython: WS2812B Addressable RGB LEDs with ESP32 and ESP8266](https://randomnerdtutorials.com/micropython-ws2812b-addressable-rgb-leds-neopixel-esp32-esp8266/)

## Hardware

Per deelnemer:

- [Wemos D1 mini (ESP8266)](https://www.tinytronics.nl/shop/nl/arduino/wemos/wemos-d1-mini-v2-esp8266-12f-ch340) 6,50
of [ESP32](https://www.tinytronics.nl/shop/nl/arduino/wemos/wemos-lolin32-esp32-cp2104) 10,50
- [hartslag sensor](https://www.tinytronics.nl/shop/nl/sensoren/optisch/hartslagsensor-xd-58c-met-accessoires) 6,00
- [0.96 inch OLED (128x64)](https://www.tinytronics.nl/shop/nl/display/oled/0.96-inch-oled-display-128*64-pixels-blauw-i2c) 7,00
- [breadboard](https://www.tinytronics.nl/shop/nl/prototyping/breadboards/breadboard-400-points-transparant) 3,00

Voor experimenteren/opzetten workshop:

- [2x breadboard draden](https://www.tinytronics.nl/shop/nl/kabels/prototype-draden/breadboard-draden-140-stuks-verschillende-maten-in-doosje) 8,00
- [Neopixel WS2813 LED strip 1 meter](https://www.tinytronics.nl/shop/nl/verlichting/led-strips/ws2813-digitale-5050-rgb-led-strip-60-leds-1m) 13,00

Totale kosten uitgaande van 5 sets:

- ESP8266: 133,50 (26,70 pp)
- ESP32: 153,50 (30,70 pp)

## Software

Een eenvoudige maar praktische IDE voor microPython, inclusief mogelijkheid om programma's naar ESP32/ESP8266 te uploaden en (terminal) output te bekijken is [Thonny IDE](https://thonny.org/).
