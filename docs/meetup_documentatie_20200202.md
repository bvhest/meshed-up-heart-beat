# TFG meetup - "Meshed-up heart-beat"

## Doel

Uitwerken van een eenvoudig project obv ongeveer drie meet-ups waarin IoT, data-uitwisseling, data-analyse en visualisatie bij elkaar komen. Hopelijk geldt dit ook voor kennis-deling en uitwisseling op dit gebied.

Wat gaan we maken?

 * Een eenvoudige hartslag-sensor die de sensor-data uitwisseld met een centrale server en de sensor-data vergelijkt met die van de andere sensoren (dag 1), 
 * De gegevensuitwisseling wordt robuster gemaakt door het gebruik van een mesh-network (dag 2),
 * Tenslotte wordt edge-verwerking op basis van een eenvoudig ML model toegevoegd (dag 3).

![Heartbeat project](./images/heartbeat-project.gif)

## Hardware/BOM

Per deelnemer:

- [ESP32](https://www.tinytronics.nl/shop/nl/arduino/wemos/wemos-lolin32-esp32-cp2104) € 10,50 (dag 1)
- [hartslag sensor](https://www.tinytronics.nl/shop/nl/sensoren/optisch/hartslagsensor-xd-58c-met-accessoires) € 6,00 (dag 1)
- [0.96 inch OLED (128x64)](https://www.tinytronics.nl/shop/nl/display/oled/0.96-inch-oled-display-128*64-pixels-blauw-i2c) € 7,00 (dag 1)
- [breadboard](https://www.tinytronics.nl/shop/nl/prototyping/breadboards/breadboard-830-points) € 4,00 (dag 1)
- [NRF24L01+ Wireless Module](https://www.tinytronics.nl/shop/nl/communicatie/rf/nrf24l01-wireless-module-zwart) € 2,50 (dag 2)


Voor experimenteren/opzetten workshop:

- [4x breadboard draden](https://www.tinytronics.nl/shop/nl/kabels/prototype-draden/breadboard-draden-140-stuks-verschillende-maten-in-doosje) € 16,00
- [2x Neopixel WS2813 LED strip 1 meter](https://www.tinytronics.nl/shop/nl/verlichting/led-strips/ws2813-digitale-5050-rgb-led-strip-60-leds-1m) € 26,00


## Benodigde software

### obv installatie

Een eenvoudige maar praktische IDE voor microPython, inclusief mogelijkheid om programma's naar ESP32/ESP8266 te uploaden en (terminal) output te bekijken is [Thonny IDE](https://thonny.org/).

### obv vm-image

Een virtuele programmeer omgeving met alle benodigde software voorgeinstalleerd is te vinden op [Sharepoint](https://mytfg-my.sharepoint.com/:u:/g/personal/dinne_bosman_the-future-group_com/EbB6jSCmfYhPjBI82cu4kG0B0Zz7fFCk8V4GAyf5o11Whg?e=u4uovS).

De omgeving bestaat uit het volgende:

1. Virtual Box - Ubuntu VM
2. Microsoft VSCode IDE
3. Docker
4. VSCode remote container plugin, Python plugin, mpfshell
5. ESP32 development docker container 

The installed Docker image contains the following:

1. xtensa-esp32 toolchain (compiled from source!) in /esp/crosstool-NG
2. esp-idf (freertos ESP32 library) in /esp/esp-idf
3. micropython in /root/micropython
    a. Micropython-wrap in /root/micropython/extmode/micropython-wrap
4. esptool (allows to erase flash)
5. mpfshell (allows to put files on the ESP32 flash, start the REPL)
6. micropy-cli (allows the setup micropython projects including intellisense and pylinting)
7. An example of a custom C++ module is added to Micropython using micropython-wrap.

#### gebruik vm-image

This repo shows how to work with Micropython on an ESP32 device. 

1. Install [VirtualBox](https://www.virtualbox.org/wiki/VirtualBox): download for [Linux](https://www.virtualbox.org/wiki/Linux_Downloads), [Mac](https://download.virtualbox.org/virtualbox/6.1.2/VirtualBox-6.1.2-135662-OSX.dmg), [Windows](https://download.virtualbox.org/virtualbox/6.1.2/VirtualBox-6.1.2-135663-Win.exe).
2. Get the VirtualBox [heartbeet-image](https://mytfg-my.sharepoint.com/:u:/g/personal/dinne_bosman_the-future-group_com/EbB6jSCmfYhPjBI82cu4kG0B0Zz7fFCk8V4GAyf5o11Whg?e=u4uovS) (7GB download, about 20 GB free disk space required)
2. In Virtualbox choose in the menu "import appliance".
3. Start the VM, login as "developer" password: "developer"
4. Open a terminal (with Ctrl+Alt+t) and run commands:

	docker restart default_esp32_vscode-developer
	cd ~/esp32/vscode_esp32
	code

VSCode starts with the example project openend
In the left lower corner a green bar is show, displaying the VSCode has connected with the container.

Open a terminal in VSCode. Run various commands to interact with the ESP32:

  a. erase flash

	esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash

  b. flash micropython firmware

	cd /root/micropython/ports/esp32 && sudo make deploy

  c. upload files

	cd /home/developer/esp32/vscode_esp32/example_project
	mpfs -n --nohelp -c "open ttyUSB0; put ssd1306.py; put main.py;"

	esptool.py --chip esp32 --port /dev/ttyUSB0 --after hard_reset chip_id

  d. Start a REPL (the REPL can be quit with CTRL+])

	mpfs -n --nohelp -c "open ttyUSB0; r;"


## DAG 1 - een online hartslagmeter

### Opbouw

De basis van de hartslagsensor is de Lolin ESP32, een krachtige microcontroller met 512kB RAM, 240Mhz dual core processor en 4MB flash geheugen.

De pin-out is als volgt:
![pinout Wemos-Lolin-ESP32](../esp32_lolin/docs/wemos_lolin32_pinout.jpg)

De signaal-uitgang van de 'Heart Rate detecting sensor' wordt aangesloten op de ADC-ingang van de ESP32. De pin-out van de sensor is als volgt:

| Pin Number | Pin Name | Wire Colour | Description|
| - | ------------ | -------- | ------------------------------------------------------------ |
| 1 | Ground | Black | Connected to the ground of the system |
| 2 | Vcc | Black | Connect to +5V or +3.3V supply voltage |
| 3 | Signal | Purple | Pulsating output signal. |

![pinout heartbeat sensor](../heartbeat_sensor/docs/heartbeat_sensor_pinout.jpg)

De meetwaarden worden, inclusief eenvoudige hart 'sprite', getoond op een OLED schermpje van 128x64 pixels. Dit schem wordt aangestuurd via een [I2C/IIC-bus](https://nl.wikipedia.org/wiki/I%C2%B2C-bus), een synchrone, seriële bus. De pin-out van het OLED scherm is als volgt:

| ssd1306 Display | Wemos LOLIN32 - ESP32| 
| --------------- | -------------------|
| GND | GND |
| VCC | 3.3V |
| SCL | GPIO22-SCL (rechts pin16) |
| SDA | GPIO21-SDA (rechts pin15) |

![pinout OLED screen](../oled_display/docs/OLED-Display-font.jpg)


Bouw de componenten op volgens onderstaand schema:

![schema heartbeatsensor](./images/heartbeatsensor-schema.jpg)



### Code

De code voor de heartbeat sensor is/wordt geschreven in [MicroPython](https://micropython.org/), een  implementatie van Python 3 die is geoptimaliseerd om op microcontrollers te werken. Net zoals bij Python wordt de code niet gecompileerd, maar tijdens het uitvoeren geïnterpreteerd.

De microPython-code wordt via een USB-verbinding op de microcontroller overgezet.

Maar voordat de MicroPython code kan worden uitgevoerd, moet de MicroPython interpreter op de ESP32 worden geinstalleerd.

#### installatie MicroPython interpreter

For complete installation instructions, see [https://micropython.org/download#esp32](https://micropython.org/download#esp32).

Quick steps:

1. Ensure [Python 3](https://www.python.org/downloads/) is installed on your computer.
1. Download the [MicroPython interpreter](https://micropython.org/resources/firmware/esp32-idf3-20191220-v1.12.bin).
1. connect the ESP32 to an USB port on your computer. Check the port-number (eg using Thonny) and replace the USB port in the commands shown below.

*Linux/Mac*

1. Download the [ESP-tool](https://github.com/espressif/esptool).
		sudo pip3 install esptool
2. If you are putting MicroPython on your board for the first time then you should first erase the entire flash using:

		esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash

3. From then on program the firmware starting at address 0x1000:

		esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 esp32-idf3-20191220-v1.12.bin


*Windows*

1. Download the [ESP-tool](https://github.com/espressif/esptool).
		py -m pip install esptool
1. Install [CP210x USB to UART Bridge VCP Drivers](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers).
2. If you are putting MicroPython on your board for the first time then you should first erase the entire flash using:

		py -m esptool --chip esp32 --port COM3 erase_flash

3. From then on program the firmware starting at address 0x1000:

		py -m esptool --chip esp32 --port COM3 --baud 460800 write_flash -z 0x1000 esp32-idf3-20191220-v1.12.bin


#### MicroPython hearbeat code

De MicroPython code voor de hearbeat sensor kan worden gedownload van deze Gitlab repository: [Meshed-up heart-beat](https://gitlab.com/bvhest/meshed-up-heart-beat). De code is te vinden in de 
 `esp32_lolin/code` directory.
 
 - `boot.py` : configuratie om verbinding te maken met het wifi en de mqtt-broker
 - `main.py` : het hoofdprogramma
 - `umqttsimple.py` : een library om gebruik te kunnen maken van het mqtt-protocol.
 
 
### MQTT in een notedop

**Wat is MQTT ?**

MQTT staat voor "Message Queuing Telemetry Transport". MQTT is een 'licht' 'publish/subscribe' protocol voor de uitwisseling van berichten over TCP/IP (het internet). Het is speciaal ontworpen voor de IOT, waarbij berichten 2 bytes groot (klein) kunnen zijn.

Berichten worden door een client (IOT-device) gepubliceerd met een label (*topic*) op de server (*broker*) waarmee ze zijn verbonden. De broker verdeeld de berichten naar andere clients die met de broker zijn verbonden en in het topic zijn geinteresseerd (*subscribed*).

Voor de uitwisseling van de berichten zijn verschillende kwaliteits niveaus (*quality-of-service*) in te stellen:

 * '0' een bericht wordt éénmaal (en niet meer) verstuurd zonder dat een ontvangstbevestiging wordt verstuurd (ofwel 'fire-and-forget').
 * '1' een bericht wordt minstens éénmaal, maar indien nodig meerdere keren verstuurd, totdat een ontvangstbevestiging wordt ontvangen ('acknowledged delivery' of 'single handshake').
 * '2' een bericht wordt precies éénmaal verstuurd, waarbij de ontvangst wordt gegarandeerd dmv een 'two level handshake' (ofwel 'assured delivery').


**Wat is nodig voor MQTT ?**

Allereerst een werkende internet-verbinding op je IOT-device.

Daarnaast een connectie met een broker. De broker kan een publieke broker zijn, of een private broker in het eigen netwerk of op het internet. 

[mosquitto](http://mosquitto.org/) is open-source software die kan worden gebruikt voor het opzetten van een broker op een eigen server, bijvoorbeeld op een Raspberry Pi. Daarnaast kan gebruik worden gemaakt van publieke brokers, zoals bijv. [HiveMQ](https://www.hivemq.com/public-mqtt-broker/), [Adafruit](io.adafruit.com) of [mosquitto](http://test.mosquitto.org).  
NB houdt er rekening mee dat de data die naar een publieke broker wordt verstuurd publiek, en dus voor iedereen toegankelijk is.

De client moet zich identificeren op de broker. Hiervoor is een 'user' en een 'key' nodig. De kunnen op de broker worden aangemaakt.

**Hoe MQTT berichten te versturen en te ontvangen ?**

Berichten worden gepubliceerd naar een topic. Clients kunnen zich ook opgeven voor een topic. Het is ook mogelijk om berichten zowel te publiceren als te ontvangen. Dit is mogelijk voor verschillende topics.

Berichten (en alle andere variabelen die te maken hebben met MQTT) worden opgeslagen als 'bytes'. Afhankelijk van de gebruikte programmeertaal moeten numerieke waarden of strings worden omgezet (ge-cast) naar bytes.


**MQTT voorbeeldcode**

MicroPython voorbeeldcode voor het opzetten van een verbinding met een MQTT server.

Allereerst de variabelen. NB de user en key/password zijn optioneel. Als deze gegevens worden verstuurd, zorg er dan voor dat ze zijn versleuteld. Brokers zoals HiveMQ kunnen clients ook authenticeren met een SSL certificaat.

	from umqttsimple import MQTTClient
	
	client_id = hexlify(machine.unique_id())
	mqtt_topic_pub = b'heartbeat'  # niet nodig voor HiveMQ
	mqtt_topic_sub = b'doctor'

	# nodig voor de HiveMQ MQTT-broker:
	mqtt_server =  'broker.hivemq.com'
	mqtt_feed = mqtt_topic_pub

	# nodig voor de Adafruit MQTT-broker:
	# see https://learn.adafruit.com/adafruit-io/mqtt-api
	mqtt_server = b'io.adafruit.com' # TCP Port: 1883 , Websocket Port: 8000
	mqtt_user = b'my-user'
	mqtt_iokey = b'51eb-and-the-rest-of-my-key'
	mqtt_feed = bytes('{:s}/groups/{:s}/csv'.format(mqtt_user, mqtt_topic_pub), 'utf-8')

Vervolgens wordt de verbinding tussen client en broker opgezet:

	client = MQTTClient(client_id=client_id,
				      server=mqtt_server,
				      user=mqtt_user,
				      password=mqtt_iokey)
	client.connect()

Dan kan een bericht van de client naar de broker worden verstuurd:

	mqtt_client.publish(mqtt_feed, msg)

Bij het afsluiten van het programma wordt de verbinding met de broker weer verbroken:

	mqtt_client.disconnect()


Voor het ontvangen van berichten moet een callback-functie worden gedefinieerd en moet deze worden opgegeven bij de MQTT connectie:

	# definieer call-back functie die wordt aangeroepen als de client een bericht ontvangt:
	def sub_callback(topic, msg):
	  if topic == mqtt_topic_sub:
	    print('ESP received message from broker')

	# als de client ook luistert, dan de verbinding met de broker als volgt opzetten:
	client = MQTTClient(client_id=client_id,
				      server=mqtt_server,
				      user=mqtt_user,
				      password=mqtt_iokey)
	client.set_callback(sub_callback)
	client.connect()
	client.subscribe(mqtt_topic_sub)



## DAG 2 - hartslagmeter in een mesh-netwerk

### Opbouw

ToDo...

### Code

ToDo...

## DAG 3 - ML op de hartslagmeter

### Opbouw

ToDo...

### Code

ToDo...


## Documentatie

  * [Finding the beat in HR sensor data](https://www.twobitarcade.net/article/wemos-heart-rate-sensor-display-micropython/)
  * Inleiding MicroPython op de ESP32: [Getting Started with MicroPython on ESP32 and ESP8266](https://randomnerdtutorials.com/getting-started-micropython-esp32-esp8266/)
  * [Introduction to MQTT](https://learn.sparkfun.com/tutorials/introduction-to-mqtt/all)
  * [MQTT essentials](https://www.hivemq.com/mqtt-essentials/)
  * [MicroPython: WS2812B Addressable RGB LEDs with ESP32 and ESP8266](https://randomnerdtutorials.com/micropython-ws2812b-addressable-rgb-leds-neopixel-esp32-esp8266/)

