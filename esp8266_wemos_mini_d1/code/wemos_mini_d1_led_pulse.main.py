#
# Wemos D1 Mini: on-board LED werkt.
#
# BvH, 12-10-2019
#

from machine import Pin, Signal, ADC

adc = ADC(0)

# On my board on = off, need to reverse.
led = Signal(Pin(2, Pin.OUT), invert=False)

MAX_HISTORY = 250

# Maintain a log of previous values to 
# determine min, max and threshold.
history = []

while True:
    v = adc.read()

    history.append(v)

    # Get the tail, up to MAX_HISTORY length
    history = history[-MAX_HISTORY:]

    minima, maxima = min(history), max(history)

    threshold_on = (minima + maxima * 3) // 4   # 3/4
    threshold_off = (minima + maxima) // 2      # 1/2

    if v > threshold_on:
        led.on()

    if v < threshold_off:
       led.off()
