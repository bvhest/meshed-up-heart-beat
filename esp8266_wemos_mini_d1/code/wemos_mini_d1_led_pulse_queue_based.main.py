#
# Wemos D1 Mini: on-board LED werkt, het OLED display ook.
#
# BvH, 12-10-2019
#

from machine import Pin, Signal, I2C, ADC, Timer
import ssd1306
import time

adc = ADC(0)

led = Signal(Pin(2, Pin.OUT), invert=False)

# our display has a resolution of 128 x 64 
# our display uses address: 0x78 
i2c = I2C(-1, scl=Pin(5), sda=Pin(4))

oled_width = 128
oled_height = 64
display = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

devices = i2c.scan()
print(devices)

# variables for measuring the heart-beat:
MAX_HISTORY = 250
TOTAL_BEATS = 30

def calculate_bpm(beats):
    # Truncate beats queue to max, then calculate bpm.
    # Calculate difference in time from the head to the
    # tail of the list. Divide the number of beats by
    # this duration (in seconds)
    beats = beats[-TOTAL_BEATS:]
    beat_time = beats[-1] - beats[0]
    if beat_time:
        display.scroll(0, -13)
        bpm = (len(beats) / (beat_time)) * 60
        display.text("%d bpm" % bpm, 12, 0)
        display.show()


def detect():
    # Maintain a log of previous values to
    # determine min, max and threshold.
    history = []
    beats = []
    beat = False

    while True:
        v = adc.read()
        history.append(v)

        # Get the tail, up to MAX_HISTORY length
        history = history[-MAX_HISTORY:]

        minima, maxima = min(history), max(history)

        threshold_on = (minima + maxima * 3) // 4   # 3/4
        threshold_off = (minima + maxima) // 2      # 1/2

        if v > threshold_on and beat == False:
            beat = True
            led.on()
            beats.append(time.time())
            # Truncate beats queue to max
            #beats = beats[-TOTAL_BEATS:]
            calculate_bpm(beats)

        if v < threshold_off and beat == True:
            beat = False
            led.off()

detect()
