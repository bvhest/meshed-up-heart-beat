# Wemos-Lolin-ESP32 Specificaties

## Features

 * WIFI
 * bluetooth
 * 4MB Flash
 * Lithium battery interface, 500mA Max charging current



## Pin configuration

![Wemos-Lolin-ESP32 voor](wemos_lolin32_front.jpg)
![Wemos-Lolin-ESP32 achter](wemos_lolin32_back.jpg)
![Wemos-Lolin-ESP32 pinout](wemos_lolin32_pinout.jpg)


## Voorbeeld projecten


## Source

  * Wemos [Lolin32](https://wiki.wemos.cc/products:lolin32:lolin32)


## Installing MicroPython on ESP32

For installation instructions, see [https://micropython.org/download#esp32](https://micropython.org/download#esp32)

If you are putting MicroPython on your board for the first time then you should first erase the entire flash using:

	esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash

From then on program the firmware starting at address 0x1000:

	esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 esp32-20190125-v1.10.bin

Check connectivity (and if MicroPython is available):

	ampy --port /dev/ttyUSB0 ls -l


## Working with Linux

Prerequisites:

* USB driver is present,
* Python is available,
* download Adafruit Ampy tool (ftp-like tool to transfer files to/from ESP32),
* download esptool to flash microPython to ESP32.


## Working with Windows

Prerequisites:

* USB driver is not present,
--> install CP210x driver (see subdirectory or [silabs.com](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers))
* Python is not available,
--> install latest version of Pyton (v3.8)
* download Adafruit Ampy tool (ftp-like tool to transfer files to/from ESP32),
* download esptool to flash microPython to ESP32.
--> install esptool with "py -m pip install esptool"
* download microPython IDE (advice: use Thonny)
In Thonny, determine the Port the ESP32 is on when connected to the computer with a micro-USB cable.


Erase flash:

	py -m esptool --chip esp32 --port COM3 erase_flash

Install microPython firmware on the ESP32:

	py -m esptool --chip esp32 --port COM3 --baud 460800 write_flash -z 0x1000 esp32-20190529-v1.11.bin
