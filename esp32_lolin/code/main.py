#
# Lolin ESP32
#
# Done:
#  - on-board LED werkt,
#  - OLED display werkt: sensor eerst op huid plaatsen, dan mcu starten.
# ToDo:
#  - opzetten Wifi/MQTT
#  - neo-pixel 'dashboard'
#  - opzetten programmeer-omgeving
#
# BvH, 16-10-2019
#

########################################################################################
# libraries
########################################################################################
from machine import Pin, Signal, I2C, ADC, Timer
import time

# using mqtt for exchanging data
from umqttsimple import MQTTClient

# using ssd1306 for controlling display
import ssd1306

# import utilities
from utils import running_mean

########################################################################################
# 'constants'
########################################################################################
OLED_WIDTH = 128
OLED_HEIGHT = 64

MAX_HISTORY = 200
TOTAL_BEATS = 30

HEART = [
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [ 0, 1, 1, 0, 0, 0, 1, 1, 0],
    [ 1, 1, 1, 1, 0, 1, 1, 1, 1],
    [ 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [ 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [ 0, 1, 1, 1, 1, 1, 1, 1, 0],
    [ 0, 0, 1, 1, 1, 1, 1, 0, 0],
    [ 0, 0, 0, 1, 1, 1, 0, 0, 0],
    [ 0, 0, 0, 0, 1, 0, 0, 0, 0],
]

########################################################################################
# variables
########################################################################################
last_y = 0


########################################################################################
# functions
########################################################################################

print("initialiseer on-board LED")
led = Signal(Pin(5, Pin.OUT), invert=True)


def initialiseer_adc():
    print("initialiseer ADC")
    # esp32: Vout from hearbeat sensor to ADC ch0 = GPIO4, pin 36.
    adc = ADC(Pin(36, Pin.IN), unit = 1) # ADC1, ch0
    # configureer bereik van 0-3.3 V (3.6 max)
    adc.atten(adc.ATTN_11DB)
    # 12 bits resolutie
    adc.width(adc.WIDTH_12BIT)

    print(adc.read())
    print("ADC klaar voor gebruik")

    return(adc)


def initialiseer_i2c():
    print("initialiseer I2C")
    # our display has a resolution of 128 x 64
    # our display uses address: 0x78
    i2c = I2C(-1, scl = Pin(22), sda = Pin(21))
    print("I2C geinitialiseerd")

    return (i2c)


def check_i2c(i2c):
    devices = i2c.scan()
    print(devices)
    print("I2C klaar voor gebruik")


def initialiseer_oled(i2c):
    print("initialiseer OLED display")
    display = ssd1306.SSD1306_I2C(OLED_WIDTH, OLED_HEIGHT, i2c)
    print("OLED display geinitialiseerd")

    return(display)


# setup MQTT connection
def mqtt_sub_cb(topic, msg):
    print((topic, msg))
    if topic == b'notification' and msg == b'received':
        print('ESP32 received a mqtt-message!')


def mqtt_connect_and_subscribe():
    print("opzetten connectie met mqtt-server")
    global mqtt_client_id, mqtt_server, mqtt_topic_sub1
    client = MQTTClient(mqtt_client_id, mqtt_server)
    client.set_callback(mqtt_sub_cb)
    client.connect()
    client.subscribe(mqtt_topic_sub1)
    print('verbonden met %s mqtt-broker, subscribed op topic %s' % (mqtt_server, mqtt_topic_sub1))
    print("connectie met mqtt-server geinitialiseerd")
    return(client)


def restart_and_reconnect():
    print('herstart van mcu...')
    time.sleep(10)
    machine.reset()


def initialiseer_mqtt():
    print("initialiseer connectie met mqtt-server")
    try:
        client = mqtt_connect_and_subscribe()

        return(client)
    except OSError as e:
        print('connectie met mqtt-server mislukt. Error=' + e)
        restart_and_reconnect()


def refresh_display(bpm, beat, v, minima, maxima):
#    print("ververs data op OLED display")
    global last_y
    global display

    display.vline(0, 0, 32, 0)
    display.scroll(-1,0) # Scroll left 1 pixel

    if maxima-minima > 0:
        # Draw beat line.
        y = 32 - int(16 * (v-minima) / (maxima-minima))
        display.line(125, last_y, 126, y, 1)
        last_y = y

    # Clear top text area.
    display.fill_rect(0,0,128,16,0) # Clear the top text area

    if bpm:
        display.text("%d bpm" % bpm, 12, 0)

    # Draw heart if beating.
    if beat:
        for y, row in enumerate(HEART):
            for x, c in enumerate(row):
                display.pixel(x, y, c)

    display.show()


def calculate_bpm(beats):
#    print("bereken bpm")
    if beats:
        # Truncate beats queue to max
        beats = beats[-TOTAL_BEATS:]
        beat_time = beats[-1] - beats[0]
        if beat_time:
            return (len(beats) / (beat_time)) * 60


def start_heart_beat_detection(mqtt_client):
    print("start oneindige loop die hart-sensor uitleest, waarde toont en verstuurd naar mqtt-broker.")
    # Maintain a log of previous values to
    # determine min, max and threshold.
    history = []
    beats = []
    beat = False
    bpm = None
    # needed for mqtt messaging
    global last_message
    global mqtt_topic_pub

    # Clear screen to start.
    display.fill(0)

    while True:
        v = adc.read()
        history.append(v)

        # Get the tail, up to MAX_HISTORY length
        history = history[-MAX_HISTORY:]

        minima, maxima = min(history), max(history)

        threshold_on = (minima + maxima * 3) // 4   # 3/4
        threshold_off = (minima + maxima) // 2      # 1/2

        if v > threshold_on and beat == False:
            beat = True
            led.on()
            beats.append(time.time())
            # Truncate beats queue to max
            #beats = beats[-TOTAL_BEATS:]
            bpm = calculate_bpm(beats)

        if v < threshold_off and beat == True:
            beat = False
            led.off()

        # show results on the OLED screen
        refresh_display(bpm, beat, v, minima, maxima)

        # send results in a message to the MQTT server
        try:
            #mqtt_client.check_msg()
            if (time.time() - last_message) > message_interval:
                msg = 'device=me1, heart-beat(bpm)=%d' % bpm
                binmsg = b'device=me1, heart-beat(bpm)=%d' % bpm
                print("publish naar mqtt-server: " + msg)
                mqtt_client.publish(mqtt_topic_pub, binmsg)
                last_message = time.time()
        except OSError as e:
            print("oops... MCU wordt herstart ivm fout " + e)
            restart_and_reconnect()



########################################################################################
# initialisatie en hoofd-loop
########################################################################################
adc = initialiseer_adc()
i2c = initialiseer_i2c()
display = initialiseer_oled(i2c)
mqtt_client = initialiseer_mqtt()

check_i2c(i2c)

# start de hoofd-loop:
start_heart_beat_detection(mqtt_client)
