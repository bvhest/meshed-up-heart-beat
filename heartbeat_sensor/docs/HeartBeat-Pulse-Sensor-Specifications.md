# Pulse Sensor Specificaties

## Features

* Biometric Pulse Rate or Heart Rate detecting sensor
* Plug and Play type sensor
* Operating Voltage: +5V or +3.3V
* Current Consumption: 4mA
* Inbuilt Amplification and Noise cancellation circuit.
* Diameter: 0.625”
* Thickness: 0.125” Thick

*Warning*: This sensor is not medical or FDA approved. It is purely intended for hobby projects/demos and should not be use for health critical applications.

 
## Pin configuration

| Pin Number | Pin Name | Wire Colour | Description|
| - | ------------ | -------- | ------------------------------------------------------------ |
| 1 | Ground | Black | Connected to the ground of the system |
| 2 | Vcc | Black | Connect to +5V or +3.3V supply voltage |
| 3 | Signal | Purple | Pulsating output signal. |


![pinout heartbeat sensor](heartbeat_sensor_pinout.jpg)

Signaal uitlezen d..m.v. ADC.


## Voorbeeld projecten

  * MicroPython: [Finding the beat in HR sensor data](https://www.twobitarcade.net/article/wemos-heart-rate-sensor-display-micropython/)
  * C++: [Pulse Sensor With Arduino Tutorial](https://www.instructables.com/id/Pulse-Sensor-With-Arduino-Tutorial/)
  * pulsesensor.com[](https://pulsesensor.com/)


## Source

  * [Components101](https://components101.com/sensors/pulse-sensor)
